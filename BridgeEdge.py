# Python program to find bridges in a given undirected graph
# Complexity : O(V+E)

from collections import defaultdict


# This class represents an undirected graph using adjacency list representation
class Graph:

    def __init__(self, vertices):
        self.V = vertices  # No. of vertices
        self.graph = defaultdict(list)  # default dictionary to store graph
        self.Time = 0

    # function to add an edge to graph
    def addEdge(self, u, v):
        self.graph[u].append(v)
        self.graph[v].append(u)

    '''A recursive function that finds and prints bridges
    using DFS traversal
    u --> The vertex to be visited next
    visited[] --> keeps track of visited vertices
    disc[] --> Stores discovery times of visited vertices
    parent[] --> Stores parent vertices in DFS tree'''

    def bridgeUtil(self, u, visited, parent, low, disc):

        # Mark the current node as visited and print it
        visited[u] = True

        # Initialize discovery time and low value
        disc[u] = self.Time
        low[u] = self.Time
        self.Time += 1

        # Recur for all the vertices adjacent to this vertex
        for v in self.graph[u]:
            # If v is not visited yet, then make it a child of u
            # in DFS tree and recur for it
            if visited[v] == False:
                parent[v] = u
                self.bridgeUtil(v, visited, parent, low, disc)

                # Check if the subtree rooted with v has a connection to
                # one of the ancestors of u
                low[u] = min(low[u], low[v])

                ''' If the lowest vertex reachable from subtree
                under v is below u in DFS tree, then u-v is
                a bridge'''
                if low[v] > disc[u]:
                    print ("%d %d" % (u, v))


            elif v != parent[u]:  # Update low value of u for parent function calls.
                low[u] = min(low[u], disc[v])

    # DFS based function to find all bridges. It uses recursive
    # function bridgeUtil()
    def bridge(self):

        # Mark all the vertices as not visited and Initialize parent and visited,
        # and ap(articulation point) arrays
        visited = [False] * (self.V)
        disc = [float("Inf")] * (self.V)
        low = [float("Inf")] * (self.V)
        parent = [-1] * (self.V)

        # Call the recursive helper function to find bridges
        # in DFS tree rooted with vertex 'i'
        for i in range(self.V):
            if visited[i] == False:
                self.bridgeUtil(i, visited, parent, low, disc)


# Create a graph given in the above diagram
g1 = Graph(5)
g1.addEdge(1, 0)
g1.addEdge(0, 2)
g1.addEdge(2, 1)
g1.addEdge(0, 3)
g1.addEdge(3, 4)

print "Bridges in first graph "
g1.bridge()

g2 = Graph(4)
g2.addEdge(0, 1)
g2.addEdge(1, 2)
g2.addEdge(2, 3)
print "\nBridges in second graph "
g2.bridge()

g3 = Graph(7)
g3.addEdge(0, 1)
g3.addEdge(1, 2)
g3.addEdge(2, 0)
g3.addEdge(1, 3)
g3.addEdge(1, 4)
g3.addEdge(1, 6)
g3.addEdge(3, 5)
g3.addEdge(4, 5)
print "\nBridges in third graph "
g3.bridge()


# A	 B	C	D	E	F	G	H	I	J	K	L	M	N	O	P	Q	R	S	T	U	V	W	X	Y	Z
# 1	 2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22	23	24	25	26

#

g4 = Graph(27)
g4.addEdge(15, 26)
g4.addEdge(26, 1)
g4.addEdge(1, 20)
g4.addEdge(20, 12)
g4.addEdge(12, 13)
g4.addEdge(13, 4)
g4.addEdge(4, 3)
g4.addEdge(1, 19)
g4.addEdge(19, 18)
g4.addEdge(18, 3)
g4.addEdge(18, 16)
g4.addEdge(19, 6)
g4.addEdge(6, 2)
g4.addEdge(16, 2)
g4.addEdge(2, 7)
g4.addEdge(2, 21)
g4.addEdge(21, 8)
g4.addEdge(8, 5)
g4.addEdge(21, 22)
g4.addEdge(22, 9)
g4.addEdge(9, 14)

print "\nBridges in fourth graph "
g4.bridge()

g5 = Graph(30)
g5.addEdge('O', 'Z')
g5.addEdge('Z', 'A')
g5.addEdge('A', 'T')
g5.addEdge('T', 'L')
g5.addEdge('L', 'M')
g5.addEdge('M', 'D')
g5.addEdge('D', 'C')
g5.addEdge('O', 'S')
g5.addEdge('A', 'S')
g5.addEdge('S', 'R')
g5.addEdge('S', 'F')
g5.addEdge('R', 'P')
g5.addEdge('R', 'C')
g5.addEdge('P', 'B')
g5.addEdge('F', 'B')
g5.addEdge('B', 'U')
g5.addEdge('U', 'H')
g5.addEdge('H', 'E')
g5.addEdge('U', 'V')
g5.addEdge('V', 'I')
g5.addEdge('I', 'N')

print "\nBridges in five graph "
g5.bridge()
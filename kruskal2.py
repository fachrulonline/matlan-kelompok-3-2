import heapq

def edge_exists(from_vertice, to_vertice, cost, existing_edges):
    for edge in existing_edges:
        if (from_vertice, to_vertice, cost) == edge or (to_vertice, from_vertice, cost) == edge:
            return True
    return False


def kruskal(graph):
    existing_edges = []
    min_heap = []
    note_to_kota = {}
    kota_to_nodes = {}
    kota_counter = 0
    for from_vertice in list(graph.keys()):
        note_to_kota[from_vertice] = kota_counter
        kota_to_nodes[kota_counter] = [from_vertice]
        kota_counter += 1
        for to_vertice in list(graph[from_vertice].keys()):
            if not edge_exists(from_vertice, to_vertice, graph[from_vertice][to_vertice], existing_edges):
                existing_edges.append((from_vertice, to_vertice, graph[from_vertice][to_vertice]))
                heapq.heappush(min_heap, (graph[from_vertice][to_vertice], (from_vertice, to_vertice)))

    mst_edges = []
    total_sum = 0
    counter = 1
    while counter < len(list(note_to_kota.keys())):
        min_record = heapq.heappop(min_heap)
        min_edge = min_record[1]
        min_edge_cost = min_record[0]
        if note_to_kota[min_edge[0]] != note_to_kota[min_edge[1]]:
            counter += 1
            mst_edges.append(min_edge)
            total_sum += min_edge_cost
            abandoned_island = note_to_kota[min_edge[1]]
            for vertice in kota_to_nodes[abandoned_island]:
                note_to_kota[vertice] = note_to_kota[min_edge[0]]
            kota_to_nodes[note_to_kota[min_edge[0]]] = kota_to_nodes[note_to_kota[min_edge[0]]] + kota_to_nodes[abandoned_island]
            del kota_to_nodes[abandoned_island]

    return (mst_edges, total_sum)

graphKotaRomania = {'A': {'Z': 75, 'T': 118, 'S': 140},
          'Z': {'A': 75, 'O': 71},
          'O': {'Z': 71, 'S': 151},
          'T': {'A': 118, 'L': 111},
          'L': {'T': 111, 'M': 70},
          'M': {'L': 70, 'D': 75},
          'D': {'M': 75, 'C': 120},
          'C': {'R': 146, 'D': 120, 'P': 138},
          'R': {'C': 146, 'P': 97, 'S': 80},
          'S': {'R': 80, 'O': 151, 'F': 99, 'A': 140},
          'F': {'S': 99, 'B': 211},
          'P': {'R': 97, 'C': 138, 'B': 101},
          'B': {'G': 90, 'F': 211, 'P': 101, 'U': 85},
          'G': {'B': 90},
          'U': {'B': 85, 'H': 98, 'V': 142},
          'H': {'U': 98, 'E': 86},
          'E': {'H': 86},
          'V': {'U': 142, 'I': 92},
          'I': {'V': 92, 'N': 87},
          'N': {'I': 87}}

print (kruskal(graphKotaRomania))